
import './App.css';
import Navbar from './Navbar';
import {BrowserRouter,Route,Routes} from 'react-router-dom';
import Home from './Home';
import About from './About';
import Contact from './Contact';
import './Navbar.css';
function App() {
  return (
  <div>
    <BrowserRouter>
      <Navbar />
      <Routes>
        <Route path="/Home" element={<Home />}/>
        <Route path="/About" element={<About/>}/>
        <Route path="/Contact" element={<Contact/>}/>
        </Routes>
    </BrowserRouter>
    </div>
  );
}

export default App;
