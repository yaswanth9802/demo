import React from "react";
import {Link} from 'react-router-dom';

const Navbar=()=>{
    return(
        <div>
         <nav class="navbar navbar-expand-lg navbar-light colour">
      <a class="navbar-brand">
        <img
          src="https://images-platform.99static.com/5vIDCgdh21_YIKmzDILTl7yZwgw=/149x12:1371x1234/500x500/top/smart/99designs-contests-attachments/82/82272/attachment_82272713"
          class="navbar-image"
          alt=""
        />
      </a>
            <h1 class="heading">Bolt Abacus</h1>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation" >
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div class="navbar-nav ml-auto">
          <Link to="/Home"><p class="nav-link active">
            Home <span class="sr-only"></span></p>
          </Link>
          <Link to="/About" ><p class="nav-link" >About Us</p></Link>
          <Link to="/Contact"><p class="nav-link" >Contact Us</p></Link>
        </div>
      </div>
    </nav>
    </div>
    )
}
export default Navbar